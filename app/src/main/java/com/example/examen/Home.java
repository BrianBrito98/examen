package com.example.examen;

import android.content.Context;
import android.os.Bundle;
import android.view.Menu;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examen.api.Api;
import com.example.examen.api.Servicios.ServicioPeticion;
import com.example.examen.viewmodel.Detalle_Discos;
import com.example.examen.viewmodel.Discos;
import com.example.examen.viewmodel.Peticion_Discos;
import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.navigation.NavigationView;
import com.google.android.material.snackbar.Snackbar;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;


import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

//import android.widget.Adapter;

public class Home extends AppCompatActivity {
    private long backPressedTime;

    RecyclerView recyclerView;
    Adapter adapter;
    List<Discos> articulos = new ArrayList<>();
    ArrayList<Discos> disco = new ArrayList<>();

    private AppBarConfiguration mAppBarConfiguration;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        recyclerView = findViewById(R.id.recyclerView);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));



        Button buscar = (Button) findViewById(R.id.button);
        final EditText nombreUsuario = (EditText) findViewById(R.id.editText);

        buscar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ServicioPeticion servicio = Api.getApi(Home.this).create(ServicioPeticion.class);
                Call<Detalle_Discos> detalleCall = servicio.getDetalles(nombreUsuario.getText().toString());
                detalleCall.enqueue(new Callback<Detalle_Discos>() {
                    @Override
                    public void onResponse(Call<Detalle_Discos> call, Response<Detalle_Discos> response) {
                        Detalle_Discos details = response.body();
                        if(details.estado == "true"){
                            articulos.clear();
                            articulos = response.body().getDisco();
                            adapter = new Adapter(Home.this,articulos);
                            recyclerView.setAdapter(adapter);
                        }
                        if(details.estado == "false"){
                            Toast.makeText(Home.this, details.detalle, Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onFailure(Call<Detalle_Discos> call, Throwable t) {
                        Toast.makeText(Home.this, "Error", Toast.LENGTH_SHORT).show();
                    }
                });
            }
        });

        //Llamamos el metodo para que se muestren los datos
        retrieveJSON(Home.this);

        //Codigo generado por el Activity
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Opcion no disponible por el momento.", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        NavigationView navigationView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        mAppBarConfiguration = new AppBarConfiguration.Builder(
                R.id.nav_home, R.id.nav_gallery, R.id.nav_slideshow)
                .setDrawerLayout(drawer)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        NavigationUI.setupActionBarWithNavController(this, navController, mAppBarConfiguration);
        NavigationUI.setupWithNavController(navigationView, navController);
    }

    //Metodo que recupera el Valor JSON para usarlo adecuadamente.
    public void retrieveJSON(final Context c){
        //Interface service para obtener la extension del web service.
        ServicioPeticion service = Api.getApi(Home.this).create(ServicioPeticion.class);
        Call<Peticion_Discos> noticiaCall = service.getDiscos("a");
        noticiaCall.enqueue(new Callback<Peticion_Discos>() {
            @Override
            public void onResponse(Call<Peticion_Discos> call, Response<Peticion_Discos> response) {
                //Obtenemos el cuerpo de nuestro view model
                Peticion_Discos request = response.body();
                //Si el valor estado retorna "true", obtendremos el valor JSON con su respectivo array
                if(request.estado.equals("true")){
                    //Usaremos un "Adapter" el cual es capaz de mostrar cada dato del arreglo por su cuenta, es usado en casos de Interfaz de usuario como cuadricula, listas o pilas.
                    articulos.clear();
                    articulos = response.body().getUsuarios();
                    adapter = new Adapter(Home.this,articulos);
                    recyclerView.setAdapter(adapter);
                }else{
                    Toast.makeText(c,"Datos Incorrectos", Toast.LENGTH_LONG).show();
                }
            }

            @Override
            public void onFailure(Call<Peticion_Discos> call, Throwable t) {
                Toast.makeText(Home.this, "Error", Toast.LENGTH_SHORT).show();
            }
        });
    }

    @Override
    public void onBackPressed(){
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.home, menu);
        return true;
    }

    @Override
    public boolean onSupportNavigateUp() {
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment);
        return NavigationUI.navigateUp(navController, mAppBarConfiguration)
                || super.onSupportNavigateUp();
    }
}
