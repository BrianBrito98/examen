package com.example.examen.viewmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Peticion_Discos {
    @SerializedName("estado")
    @Expose
    public String estado;

    @SerializedName("discos")
    @Expose
    private List<Discos> discos;

    public String getStatus() {
        return estado;
    }

    public void setStatus(String estado) {
        this.estado = estado;
    }

    public List<Discos> getUsuarios() {
        return discos;
    }

    public void setUsuarios(List<Discos> discos) {
        this.discos = discos;
    }
}
