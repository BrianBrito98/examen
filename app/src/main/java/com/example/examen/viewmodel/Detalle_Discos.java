package com.example.examen.viewmodel;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class Detalle_Discos {
    public String estado;
    public List<Discos> disco;
    public String detalle;

    public Detalle_Discos(List<Discos> disco){
        this.disco = disco;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }

    public List<Discos> getDisco() {
        return disco;
    }

    public void setDisco(List<Discos> disco) {
        this.disco = disco;
    }

    public String getDetalle() {
        return detalle;
    }

    public void setDetalle(String detalle) {
        this.detalle = detalle;
    }
}
