package com.example.examen;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.example.examen.viewmodel.Discos;

import java.util.List;

public class Adapter extends RecyclerView.Adapter<Adapter.ViewHolder> {

    //Declaramos una lista de alumnos y le asignamos el contexto
    private List<Discos> articulos;
    private Context context;

    //Utiizamos el metodo constructor
    public Adapter(Context context, List<Discos> articulos) {
        this.context = context; //si
        this.articulos = articulos;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.items, parent, false);
        return new ViewHolder(view);
    }

    //Metodo "Enlazar" el cual asignara los datos del XML a sus respectivos datos en formato JSON.
    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        Discos a = articulos.get(position);
        holder.tvId.setText(a.getId());
        holder.tvTitle.setText(a.getNombre());
        holder.tvDate.setText(a.getAlbum());
        holder.tvSource.setText(a.getAnio());
    }

    //Declaramos nuestros valores XML para su uso en "onBindViewHolder"
    @Override
    public int getItemCount() {
        return articulos.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView tvId, tvTitle, tvSource, tvDate;
        ImageView imageView;
        CardView cardView;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            tvId = itemView.findViewById(R.id.tvId);
            tvTitle = itemView.findViewById(R.id.tvTitle);
            tvSource = itemView.findViewById(R.id.tvSource);
            tvDate = itemView.findViewById(R.id.tvDate);
            imageView = itemView.findViewById(R.id.image);
            cardView = itemView.findViewById(R.id.cardView);
        }
    }
}
