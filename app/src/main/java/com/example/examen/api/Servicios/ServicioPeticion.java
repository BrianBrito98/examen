package com.example.examen.api.Servicios;

import com.example.examen.viewmodel.Detalle_Discos;
import com.example.examen.viewmodel.Peticion_Discos;
import com.example.examen.viewmodel.Peticion_Login;
import com.example.examen.viewmodel.Registro_Usuario;
import com.example.examen.viewmodel.Peticion_Login;
import com.example.examen.viewmodel.Registro_Usuario;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.POST;

public interface ServicioPeticion {

    @FormUrlEncoded
    @POST("api/loginSocial")
    Call<Peticion_Login> getLogin(@Field("username") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/crearUsuario")
    Call<Registro_Usuario> signupUser(@Field("username") String email, @Field("password") String password);

    @FormUrlEncoded
    @POST("api/todosDiscos")
    Call<Peticion_Discos> getDiscos(@Field("a") String disco);

    @FormUrlEncoded
    @POST("api/detalleDisco")
    Call<Detalle_Discos> getDetalles(@Field("discoId") String discos);
}
